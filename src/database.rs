use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use std::collections::{HashMap, HashSet};
use std::default::Default;
use thiserror::Error;


#[derive(Debug, Error)]
pub enum EntryError {
    #[error(transparent)]
    Base64Decode(#[from] base64::DecodeError),
    #[error(transparent)]
    Utf8(#[from] std::string::FromUtf8Error),
}

#[derive(Debug, Error)]
pub enum GroupError {
    #[error(transparent)]
    GroupAlreadyExists(#[from] GroupAlreadyExists),
    #[error(transparent)]
    EntryAlreadyExists(#[from] EntryAlreadyExists),
    #[error(transparent)]
    GroupDoesNotExist(#[from] GroupDoesNotExist),
    #[error(transparent)]
    EntryDoesNotExist(#[from] EntryDoesNotExist),
    #[error(transparent)]
    HasChildren(#[from] GroupHasChildren),
}

#[derive(Debug, Error)]
#[error("there is already a group at `{path:?}`")]
pub struct GroupAlreadyExists {
    path: Vec<String>,
}

impl GroupAlreadyExists {
    fn merge<A: AsRef<str>, I: Into<String>>(path: &[A], name: I) -> Self {
        let mut path = path
            .iter()
            .map(AsRef::as_ref)
            .map(String::from)
            .collect::<Vec<_>>();

        path.push(name.into());

        GroupAlreadyExists { path }
    }
}

#[derive(Debug, Error)]
#[error("there is already an entry named `{name}` at `{group:?}`")]
pub struct EntryAlreadyExists {
    group: Vec<String>,
    name: String,
}

impl EntryAlreadyExists {
    fn new<S: AsRef<str>>(group: &[S], name: &str) -> EntryAlreadyExists {
        let group = group.iter().map(AsRef::as_ref).map(String::from).collect();
        let name = name.into();

        EntryAlreadyExists { group, name }
    }
}

#[derive(Debug, Error)]
#[error("there is no group at `{path:?}`")]
pub struct GroupDoesNotExist {
    path: Vec<String>,
}

impl GroupDoesNotExist {
    fn merge<A: AsRef<str>, I: Into<String>>(path: &[A], name: I) -> Self {
        let mut path = path
            .iter()
            .map(AsRef::as_ref)
            .map(String::from)
            .collect::<Vec<_>>();

        path.push(name.into());

        GroupDoesNotExist { path }
    }
}

#[derive(Debug, Error)]
#[error("there is no entry called `{name}` at `{group:?}`")]
pub struct EntryDoesNotExist {
    group: Vec<String>,
    name: String,
}

impl EntryDoesNotExist {
    fn new<S: AsRef<str>>(group: &[S], name: &str) -> EntryDoesNotExist {
        let group = group.iter().map(AsRef::as_ref).map(String::from).collect();
        let name = name.into();

        EntryDoesNotExist { group, name }
    }
}

impl<S: AsRef<str>> From<&[S]> for GroupDoesNotExist {
    fn from(path: &[S]) -> Self {
        GroupDoesNotExist {
            path: path.iter().map(AsRef::as_ref).map(String::from).collect(),
        }
    }
}

#[derive(Debug, Error)]
#[error("the group at `{path:?}` has children")]
pub struct GroupHasChildren {
    path: Vec<String>,
}

impl GroupHasChildren {
    fn merge<A: AsRef<str>, I: Into<String>>(path: &[A], name: I) -> Self {
        let mut path = path
            .iter()
            .map(AsRef::as_ref)
            .map(String::from)
            .collect::<Vec<_>>();

        path.push(name.into());

        GroupHasChildren { path }
    }
}

#[derive(Debug, Error)]
pub enum HintParseError {
    #[error("`{hint}` did not match any known hints")]
    UnrecognisedHint { hint: String },
}


/// Newtype for necessary trait implementations.
#[derive(Debug)]
struct Mime(mime::Mime);

impl schemars::JsonSchema for Mime {
    fn schema_name() -> String {
        "Mime".into()
    }

    fn json_schema(generator: &mut schemars::gen::SchemaGenerator) -> schemars::schema::Schema {
        // TODO: Add validation that this is actually a valid MIME type.
        // TODO: Add a description.
        generator.subschema_for::<String>()
    }
}

impl Serialize for Mime {
    fn serialize<S: serde::Serializer>(&self, serializer: S) -> Result<S::Ok, S::Error> {
        let mut string = String::new();

        string.push_str(self.0.type_().as_str());
        string.push('/');
        string.push_str(self.0.subtype().as_str());

        if let Some(suffix) = self.0.suffix() {
            string.push('+');
            string.push_str(suffix.as_str());
        }

        for (key, value) in self.0.params() {
            string.push_str("; ");
            string.push_str(key.as_str());
            string.push('=');
            string.push_str(value.as_str());
        }

        serializer.serialize_str(&string)
    }
}

impl<'de> Deserialize<'de> for Mime {
    fn deserialize<D: serde::Deserializer<'de>>(deserializer: D) -> Result<Self, D::Error> {
        struct MimeVisitor;

        impl<'de> serde::de::Visitor<'de> for MimeVisitor {
            type Value = Mime;

            fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
                write!(formatter, "a valid RFC 2045 MIME string")
            }

            fn visit_str<E: serde::de::Error>(self, value: &str) -> Result<Self::Value, E> {
                value.parse().map(Mime).map_err(|_| {
                    serde::de::Error::invalid_value(serde::de::Unexpected::Str(value), &self)
                })
            }
        }

        deserializer.deserialize_str(MimeVisitor)
    }
}


/// Hints about how a given field should be handled.
#[derive(Clone, Debug, Deserialize, Eq, Hash, JsonSchema, PartialEq, Serialize)]
pub enum Hint {
    /// This field contains sensitive information.
    ///
    /// UIs should ensure that this field is hidden by default.
    Sensitive,
}

impl std::str::FromStr for Hint {
    type Err = HintParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match &s.to_lowercase()[..] {
            "sensitive" => Ok(Hint::Sensitive),
            _ => Err(HintParseError::UnrecognisedHint { hint: s.into() }),
        }
    }
}

#[derive(Debug, Eq, PartialEq)]
pub enum FieldValue<'u> {
    Utf8(&'u str),
    Binary(Vec<u8>),
}

impl<'u> FieldValue<'u> {
    pub fn into_bytes(self) -> Vec<u8> {
        match self {
            FieldValue::Utf8(string) => string.to_string().into_bytes(),
            FieldValue::Binary(bytes) => bytes,
        }
    }
}

#[derive(Debug, Deserialize, JsonSchema, Serialize)]
pub struct Field {
    // TODO: Add validation that this is a set.
    /// Hints about how this field should be handled.
    hints: HashSet<Hint>,
    /// The RFC 3548 MIME type of this field.
    mime: Mime,
    // TODO: Add validation that this is base64 if mime does not have `charset=utf-8`.
    /// The value of this field.
    ///
    /// If `mime` has a `charset` parameter of `utf-8` then this value will be
    /// stored unchanged. Otherwise it will be stored base64 encoded using the
    /// standard encoding defined by RFC 3548 (_with_ padding).
    value: String,
}

impl Field {
    pub fn new(mime: mime::Mime, value: Vec<u8>) -> Result<Field, std::string::FromUtf8Error> {
        Field::with_hints(mime, value, Default::default())
    }

    pub fn with_hints(
        mime: mime::Mime,
        value: Vec<u8>,
        hints: HashSet<Hint>,
    ) -> Result<Field, std::string::FromUtf8Error> {
        let value = if mime
            .get_param("charset")
            .map(|charset| charset == "utf-8")
            .unwrap_or(false)
        {
            String::from_utf8(value)?
        } else {
            base64::encode(&value)
        };
        Ok(Field {
            hints,
            mime: Mime(mime),
            value,
        })
    }

    pub fn hints(&self) -> &HashSet<Hint> {
        &self.hints
    }

    pub fn mime(&self) -> &mime::Mime {
        &self.mime.0
    }

    pub fn value(&self) -> Result<FieldValue, base64::DecodeError> {
        if self
            .mime
            .0
            .get_param("charset")
            .map(|charset| charset == "utf-8")
            .unwrap_or(false)
        {
            Ok(FieldValue::Utf8(self.value.as_ref()))
        } else {
            base64::decode(&self.value).map(FieldValue::Binary)
        }
    }
}

#[derive(Debug)]
pub struct EntryBuilder {
    fields: HashMap<String, Field>,
    parent: Option<usize>,
}

impl Default for EntryBuilder {
    fn default() -> Self {
        EntryBuilder {
            fields: Default::default(),
            parent: None,
        }
    }
}

impl EntryBuilder {
    pub fn copy(from: usize, database: &Database) -> Result<EntryBuilder, EntryError> {
        let parent = &database.store[from];

        let mut builder = EntryBuilder::default();
        builder.parent = Some(from);
        for (name, field) in &parent.fields {
            builder.insert(
                name,
                Field::with_hints(
                    field.mime().clone(),
                    field.value()?.into_bytes(),
                    field.hints().iter().cloned().collect(),
                )?,
            );
        }

        Ok(builder)
    }

    pub fn insert<S: Into<String>>(&mut self, name: S, field: Field) -> Option<Field> {
        self.fields.insert(name.into(), field)
    }

    pub fn remove<S: AsRef<str>>(&mut self, name: S) -> Option<Field> {
        self.fields.remove(name.as_ref())
    }

    pub fn store(self, database: &mut Database) -> usize {
        let index = database.store.len();

        let EntryBuilder { fields, parent } = self;

        database.store.push(Entry { fields, parent });

        index
    }
}

#[derive(Debug, Deserialize, JsonSchema, Serialize)]
pub struct Entry {
    fields: HashMap<String, Field>,
    parent: Option<usize>,
}

impl Entry {
    pub fn fields(&self) -> &HashMap<String, Field> {
        &self.fields
    }

    pub fn parent(&self) -> Option<&usize> {
        self.parent.as_ref()
    }
}

/// A tree of tags that point to entries in the database store.
///
/// This type derives clone to make undoing changes easier.
#[derive(Clone, Debug, Default, Deserialize, JsonSchema, Serialize)]
pub struct Group {
    groups: HashMap<String, Group>,
    // TODO: Add validation that this is a set.
    entries: HashMap<String, usize>,
}

impl Group {
    pub fn groups(&self) -> &HashMap<String, Group> {
        &self.groups
    }

    pub fn entries(&self) -> &HashMap<String, usize> {
        &self.entries
    }

    // TODO: Can the following two methods be merged? E.g. by having `at` call
    // `at_mut` then downgrading the reference.
    pub fn at<S: AsRef<str>>(&self, path: &[S]) -> Option<&Group> {
        let mut group = self;

        for name in path {
            group = group.groups.get(name.as_ref())?;
        }

        Some(group)
    }

    pub fn at_mut<S: AsRef<str>>(&mut self, path: &[S]) -> Option<&mut Group> {
        let mut group = self;

        for name in path {
            group = group.groups.get_mut(name.as_ref())?;
        }

        Some(group)
    }

    fn at_mut_or_create<S: AsRef<str>>(&mut self, path: &[S]) -> &mut Group {
        let mut group = self;

        for name in path {
            // TODO: Can we get rid of this unnecessary allocation?
            group = group
                .groups
                .entry(name.as_ref().to_string())
                .or_insert_with(Default::default);
        }

        group
    }

    /// Add a new child group to this group.
    #[allow(clippy::map_entry)]
    pub fn insert_child<A: AsRef<str>, I: Into<String>>(
        &mut self,
        path: &[A],
        name: I,
    ) -> Result<(), GroupAlreadyExists> {
        let group = &mut self.at_mut_or_create(path).groups;
        let name = name.into();

        if group.contains_key(&name) {
            Err(GroupAlreadyExists::merge(path, name))
        } else {
            group.insert(name, Group::default());
            Ok(())
        }
    }

    /// Remove a child group from this group.
    pub fn remove_child<S: AsRef<str>>(
        &mut self,
        path: &[S],
        name: &str,
    ) -> Result<(), GroupError> {
        self.at_mut(path)
            .ok_or_else(|| GroupError::from(GroupDoesNotExist::from(path)))
            .and_then(|group| {
                if !group.groups.contains_key(name) {
                    Err(GroupError::from(GroupDoesNotExist::merge(path, name)))
                } else if !group.groups[name].groups.is_empty()
                    || !group.groups[name].entries.is_empty()
                {
                    Err(GroupError::from(GroupHasChildren::merge(path, name)))
                } else {
                    Ok(group.groups.remove(name))
                }
            })
            .map(|_| ())
    }

    /// Remove a child group from this group, even if it's not empty.
    pub fn remove_child_and_grandchildren<S: AsRef<str>>(
        &mut self,
        path: &[S],
        name: &str,
    ) -> Result<(), GroupDoesNotExist> {
        self.at_mut(path)
            .ok_or_else(|| GroupDoesNotExist::from(path))
            .and_then(|group| {
                if !group.groups.contains_key(name) {
                    Err(GroupDoesNotExist::merge(path, name))
                } else {
                    Ok(group.groups.remove(name))
                }
            })
            .map(|_| ())
    }

    /// Insert a new entry into this group.
    pub fn insert<S: AsRef<str>>(
        &mut self,
        group: &[S],
        name: &str,
        entry: usize,
    ) -> Result<(), EntryAlreadyExists> {
        let entries = &mut self.at_mut_or_create(group).entries;

        if entries.contains_key(name) {
            Err(EntryAlreadyExists::new(group, name))
        } else {
            entries.insert(name.into(), entry);
            Ok(())
        }
    }

    /// Remove the given entry from this group.
    pub fn remove<S: AsRef<str>>(&mut self, path: &[S], name: &str) -> Result<(), GroupError> {
        self.at_mut(path)
            .ok_or_else(|| GroupError::from(GroupDoesNotExist::from(path)))
            .and_then(|group| {
                group
                    .entries
                    .remove(name)
                    .ok_or_else(|| GroupError::from(EntryDoesNotExist::new(path, name)))
                    .map(|_| ())
            })
    }
}

#[derive(Debug, Default, Deserialize, JsonSchema, Serialize)]
pub struct Database {
    store: Vec<Entry>,
    pub hierarchy: Group,
}

impl Database {
    pub fn entries(&self) -> &[Entry] {
        &self.store
    }
}
