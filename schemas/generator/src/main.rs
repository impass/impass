use anyhow::Context;
use clap::{crate_authors, crate_description, crate_name, crate_version};

fn main() -> anyhow::Result<()> {
    let matches = clap::app_from_crate!()
        .help_message("Print help information.")
        .version_message("Print version information.")
        .arg(
            clap::Arg::with_name("directory")
                .short("o")
                .long("directory")
                .help("Directory to save the schemas to.")
                .required_unless("show")
                .conflicts_with("show")
                .requires("save")
                .takes_value(true)
                .value_name("DIRECTORY"),
        )
        .arg(
            clap::Arg::with_name("explicit")
                .short("x")
                .long("explicit")
                .help("Write the schemas to an explicit version, overwriting if necessary.")
                .takes_value(true)
                .value_name("MAJOR.MINOR")
                .validator(|string| {
                    if regex::Regex::new(r"^\d+\.\d+$").unwrap().is_match(&string) {
                        Ok(())
                    } else {
                        Err("value should be of the form <major>.<minor>".into())
                    }
                }),
        )
        .arg(
            clap::Arg::with_name("major")
                .short("a")
                .long("major")
                .help("Increase the major version number."),
        )
        .arg(
            clap::Arg::with_name("minor")
                .short("i")
                .long("minor")
                .help("Increase the minor version number for the given (or latest) major version.")
                .takes_value(true)
                .min_values(0)
                .max_values(1)
                .value_name("MAJOR")
                .validator(|string| {
                    if string.chars().all(|c| c.is_digit(10)) {
                        Ok(())
                    } else {
                        Err("value should be an integer".into())
                    }
                }),
        )
        .arg(
            clap::Arg::with_name("encrypted")
                .short("e")
                .long("encrypted")
                .help("Print the schema for the encrypted database."),
        )
        .arg(
            clap::Arg::with_name("database")
                .short("d")
                .long("database")
                .help("Print the schema for the decrypted database."),
        )
        .group(clap::ArgGroup::with_name("save").args(&["explicit", "major", "minor"]))
        .group(
            clap::ArgGroup::with_name("show")
                .args(&["encrypted", "database"])
                .multiple(true),
        )
        .get_matches();

    if matches.is_present("directory") {
        save(&matches)
    } else {
        let (mut estdout, mut esink);
        let encrypted: &mut dyn std::io::Write =
            if matches.is_present("encrypted") || !matches.is_present("database") {
                estdout = std::io::stdout();
                &mut estdout
            } else {
                esink = std::io::sink();
                &mut esink
            };

        let (mut dstdout, mut dsink);
        let database: &mut dyn std::io::Write =
            if matches.is_present("database") || !matches.is_present("encrypted") {
                dstdout = std::io::stdout();
                &mut dstdout
            } else {
                dsink = std::io::sink();
                &mut dsink
            };

        show(encrypted, database)
    }
}

fn show(
    mut encrypted: &mut dyn std::io::Write,
    mut database: &mut dyn std::io::Write,
) -> anyhow::Result<()> {
    serde_json::to_writer_pretty(&mut encrypted, &schemars::schema_for!(impass::Encrypted))?;
    writeln!(encrypted)?;

    serde_json::to_writer_pretty(&mut database, &schemars::schema_for!(impass::Database))?;
    writeln!(database)?;

    Ok(())
}

fn save(matches: &clap::ArgMatches) -> anyhow::Result<()> {
    let directory = std::path::Path::new(matches.value_of("directory").unwrap());
    std::fs::create_dir_all(directory)
        .with_context(|| format!("Unable to create directory: {}", directory.display()))?;

    let regex = regex::Regex::new(r"^(?P<major>\d+)\.(?P<minor>\d+)$").unwrap();
    let versions = std::fs::read_dir(directory)?
        .collect::<Result<Vec<_>, _>>()?
        .iter()
        .map(std::fs::DirEntry::file_name)
        .map(|name| {
            if let Some(name) = name.to_str() {
                if let Some(captures) = regex.captures(name) {
                    Some((
                        captures
                            .name("major")
                            .unwrap()
                            .as_str()
                            .parse::<u32>()
                            .unwrap(),
                        captures
                            .name("minor")
                            .unwrap()
                            .as_str()
                            .parse::<u32>()
                            .unwrap(),
                    ))
                } else {
                    None
                }
            } else {
                None
            }
        })
        .filter_map(|v| v)
        .collect::<Vec<_>>();

    let (major, minor) = if let Some(version) = matches.value_of("explicit") {
        let captures = regex.captures(version).unwrap();
        (
            captures
                .name("major")
                .unwrap()
                .as_str()
                .parse::<u32>()
                .unwrap(),
            captures
                .name("minor")
                .unwrap()
                .as_str()
                .parse::<u32>()
                .unwrap(),
        )
    } else if matches.is_present("major") {
        (
            versions
                .iter()
                .map(|(a, _)| *a)
                .max()
                .map(|v| v + 1)
                .unwrap_or(0),
            0,
        )
    } else if let Some(major) = matches.value_of("minor") {
        let major = major.parse::<u32>().unwrap();
        (
            major,
            versions
                .iter()
                .filter(|(a, _)| a == &major)
                .map(|(_, i)| *i)
                .max()
                .map(|v| v + 1)
                .unwrap_or(0),
        )
    } else if matches.is_present("minor") {
        let major = versions.iter().map(|(a, _)| *a).max().unwrap_or(0);
        (
            major,
            versions
                .iter()
                .filter(|(a, _)| a == &major)
                .map(|(_, i)| *i)
                .max()
                .map(|v| v + 1)
                .unwrap_or(0),
        )
    } else {
        unreachable!();
    };

    let version = format!("{}.{}", major, minor);
    let directory = directory.join(version);
    let mut options = std::fs::OpenOptions::new();
    options.write(true).truncate(true).create(true);

    std::fs::create_dir_all(&directory)
        .with_context(|| format!("Unable to create directory: {}", directory.display()))?;

    let encrypted = directory.join("encrypted.json");
    let database = directory.join("database.json");

    show(
        &mut options
            .open(&encrypted)
            .with_context(|| format!("Unable to create file: {}", encrypted.display()))?,
        &mut options
            .open(&database)
            .with_context(|| format!("Unable to create file: {}", database.display()))?,
    )
}
