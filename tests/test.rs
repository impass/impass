// TODO: Use tests that return results instead of `unwrap`.

#[test]
fn parsing_correct_hint() {
    assert_eq!(
        "SeNsItIvE".parse::<impass::database::Hint>().unwrap(),
        impass::database::Hint::Sensitive
    );
}

#[test]
#[should_panic]
fn parsing_incorrect_hint() {
    "this is not a hint"
        .parse::<impass::database::Hint>()
        .unwrap();
}

#[test]
fn creating_text_field() {
    let field = impass::Field::new(
        impass::mime::TEXT_PLAIN_UTF_8,
        "hello".to_string().into_bytes(),
    )
    .unwrap();
    assert_eq!(
        field.value().unwrap(),
        impass::database::FieldValue::Utf8("hello")
    );
}

#[test]
fn creating_binary_field() {
    let field = impass::Field::new(
        impass::mime::APPLICATION_OCTET_STREAM,
        b"\x01\x02\x03".to_vec(),
    )
    .unwrap();
    assert_eq!(
        field.value().unwrap(),
        impass::database::FieldValue::Binary(b"\x01\x02\x03".to_vec())
    );
}

#[test]
fn creating_database() {
    let database = impass::Database::default();
    assert!(database.hierarchy.groups().is_empty() && database.hierarchy.entries().is_empty());
}

#[test]
fn adding_an_entry_with_a_text_field() {
    let mut database = impass::Database::default();

    let mut builder = impass::EntryBuilder::default();
    builder.insert(
        "Bar",
        impass::Field::new(impass::mime::TEXT_PLAIN_UTF_8, b"Hello world!".to_vec()).unwrap(),
    );
    let index = builder.store(&mut database);

    let entry = &database.entries()[index];
    let field = entry.fields().get("Bar").unwrap();
    assert!(
        field.hints().is_empty()
            && field.mime() == &impass::mime::TEXT_PLAIN_UTF_8
            && String::from_utf8(field.value().unwrap().into_bytes()).unwrap() == "Hello world!"
    );
}

#[test]
fn adding_an_entry_with_a_binary_field() {
    let mut database = impass::Database::default();

    let mut builder = impass::EntryBuilder::default();
    builder.insert(
        "Bar",
        impass::Field::new(
            impass::mime::APPLICATION_OCTET_STREAM,
            b"\x01\x02\x03".to_vec(),
        )
        .unwrap(),
    );
    let index = builder.store(&mut database);

    let entry = &database.entries()[index];
    let field = entry.fields().get("Bar").unwrap();
    assert!(
        field.hints().is_empty()
            && field.mime() == &impass::mime::APPLICATION_OCTET_STREAM
            && field.value().unwrap().into_bytes() == b"\x01\x02\x03"
    );
}

#[test]
fn copying_an_entry() {
    let mut database = impass::Database::default();

    let mut builder = impass::EntryBuilder::default();
    builder.insert(
        "Bar",
        impass::Field::new(impass::mime::TEXT_PLAIN_UTF_8, b"Hello world!".to_vec()).unwrap(),
    );
    let parent = builder.store(&mut database);

    let mut builder = impass::EntryBuilder::copy(parent, &database).unwrap();
    builder.insert(
        "Baz",
        impass::Field::new(
            impass::mime::APPLICATION_OCTET_STREAM,
            b"\x01\x02\x03".to_vec(),
        )
        .unwrap(),
    );
    let index = builder.store(&mut database);

    let entry = &database.entries()[index];
    let bar = entry.fields().get("Bar").unwrap();
    let baz = entry.fields().get("Baz").unwrap();
    assert!(
        entry.parent() == Some(&parent)
            && bar.hints().is_empty()
            && bar.mime() == &impass::mime::TEXT_PLAIN_UTF_8
            && String::from_utf8(bar.value().unwrap().into_bytes()).unwrap() == "Hello world!"
            && baz.hints().is_empty()
            && baz.mime() == &impass::mime::APPLICATION_OCTET_STREAM
            && baz.value().unwrap().into_bytes() == b"\x01\x02\x03"
    );
}

#[test]
fn adding_an_entry_to_a_group() {
    let mut database = impass::Database::default();

    let mut builder = impass::EntryBuilder::default();
    builder.insert(
        "Bar",
        impass::Field::new(impass::mime::TEXT_PLAIN_UTF_8, b"Hello world!".to_vec()).unwrap(),
    );
    let index = builder.store(&mut database);

    database
        .hierarchy
        .insert(&["a", "b", "c", "d"], "Foo", index)
        .unwrap();

    assert_eq!(
        database
            .hierarchy
            .at(&["a", "b", "c", "d"])
            .unwrap()
            .entries()
            .get("Foo")
            .unwrap(),
        &index,
    )
}

#[test]
fn adding_an_entry_to_a_group_with_path_of_strings() {
    let mut database = impass::Database::default();

    let mut builder = impass::EntryBuilder::default();
    builder.insert(
        "Bar",
        impass::Field::new(impass::mime::TEXT_PLAIN_UTF_8, b"Hello world!".to_vec()).unwrap(),
    );
    let index = builder.store(&mut database);

    let path = vec![
        String::from("a"),
        String::from("b"),
        String::from("c"),
        String::from("d"),
    ];

    database.hierarchy.insert(&path, "Foo", index).unwrap();

    assert_eq!(
        database
            .hierarchy
            .at(&path)
            .unwrap()
            .entries()
            .get("Foo")
            .unwrap(),
        &index,
    )
}
#[test]
fn adding_an_entry_to_two_groups() {
    let mut database = impass::Database::default();

    let mut builder = impass::EntryBuilder::default();
    builder.insert(
        "Bar",
        impass::Field::new(impass::mime::TEXT_PLAIN_UTF_8, b"Hello world!".to_vec()).unwrap(),
    );
    let index = builder.store(&mut database);

    database
        .hierarchy
        .insert(&["a", "b", "c", "d"], "Foo", index)
        .unwrap();
    database
        .hierarchy
        .insert(&["a", "c"], "Foo", index)
        .unwrap();

    assert_eq!(
        database
            .hierarchy
            .at(&["a", "b", "c", "d"])
            .unwrap()
            .entries(),
        database.hierarchy.at(&["a", "c"]).unwrap().entries(),
    );
}


#[test]
fn round_trip() {
    let mut buffer = Vec::new();


    let mut database = impass::Database::default();

    let mut builder = impass::EntryBuilder::default();
    builder.insert(
        "Bar",
        impass::Field::new(impass::mime::TEXT_PLAIN_UTF_8, b"Hello world!".to_vec()).unwrap(),
    );
    let index = builder.store(&mut database);

    database
        .hierarchy
        .insert(&["a", "b", "c", "d"], "Foo", index)
        .unwrap();
    database
        .hierarchy
        .insert(&["a", "e"], "Foo", index)
        .unwrap();

    let encrypted = impass::Encrypted::encrypt(
        impass::PersistentMetadata::default().with_aes256(10),
        database,
        "abc123".as_bytes(),
    )
    .unwrap();

    encrypted.save(&mut buffer).unwrap();


    // Database has now been saved to "file".


    let encrypted = impass::Encrypted::load(&buffer[..]).unwrap();

    let decrypted = encrypted.decrypt("abc123".as_bytes()).unwrap();

    assert!(
        decrypted.database.entries().len() == 1
            && decrypted
                .database
                .hierarchy
                .at(&["a", "b", "c", "d"])
                .unwrap()
                .entries()
                == decrypted
                    .database
                    .hierarchy
                    .at(&["a", "e"])
                    .unwrap()
                    .entries()
    );
}


#[test]
fn database_validates_against_schema() {
    let mut database = impass::Database::default();

    let mut builder = impass::EntryBuilder::default();
    builder.insert(
        "Bar",
        impass::Field::new(impass::mime::TEXT_PLAIN_UTF_8, b"Hello world!".to_vec()).unwrap(),
    );
    builder.insert(
        "Baz",
        impass::Field::new(
            impass::mime::APPLICATION_OCTET_STREAM,
            b"\x01\x02\x03".to_vec(),
        )
        .unwrap(),
    );
    let index = builder.store(&mut database);

    database
        .hierarchy
        .insert(&["a", "b", "c"], "Foo", index)
        .unwrap();
    database
        .hierarchy
        .insert(&["d", "a"], "Foo", index)
        .unwrap();

    let json = serde_json::to_value(database).unwrap();
    let schema = serde_json::from_str::<serde_json::Value>(include_str!(
        "../schemas/versions/0.0/database.json"
    ))
    .unwrap();
    let mut scope = valico::json_schema::Scope::new();
    let validator = scope.compile_and_return(schema, true).unwrap();

    assert!(validator.validate(&json).is_valid());
}

#[test]
fn encrypted_file_validates_against_schema() {
    let mut database = impass::Database::default();

    let mut builder = impass::EntryBuilder::default();
    builder.insert(
        "Bar",
        impass::Field::new(impass::mime::TEXT_PLAIN_UTF_8, b"Hello world!".to_vec()).unwrap(),
    );
    builder.insert(
        "Baz",
        impass::Field::new(
            impass::mime::APPLICATION_OCTET_STREAM,
            b"\x01\x02\x03".to_vec(),
        )
        .unwrap(),
    );
    let index = builder.store(&mut database);

    database
        .hierarchy
        .insert(&["a", "b", "c"], "Foo", index)
        .unwrap();
    database
        .hierarchy
        .insert(&["d", "a"], "Foo", index)
        .unwrap();

    let encrypted = impass::Encrypted::encrypt(
        impass::PersistentMetadata::default().with_aes256(1),
        database,
        "abc123".as_bytes(),
    )
    .unwrap();

    let json = serde_json::to_value(encrypted).unwrap();
    let schema = serde_json::from_str::<serde_json::Value>(include_str!(
        "../schemas/versions/0.0/encrypted.json"
    ))
    .unwrap();
    let mut scope = valico::json_schema::Scope::new();
    let validator = scope.compile_and_return(schema, true).unwrap();

    assert!(validator.validate(&json).is_valid());
}

// TODO: Add test where password is wrong!

// TODO: Add test where you try to create a field with a UTF-8 MIME but an
// invalid UTF-8 string.

//TODO: Add test where you implicitly create a new group
// using `insert`.

// TODO: Set up coverage.
